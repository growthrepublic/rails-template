require 'net/http'

# GEMS
## assets
gem 'bootstrap-sass'
gem 'lodash-rails'
gem 'sugar-rails'
gem 'assets_js'
gem 'gon'

## development and debugging
gem 'pry-rails'
gem_group :development do
  gem 'annotate'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'quiet_assets'
  gem 'pry-byebug'
  gem 'pry-nav'
end

## tests
gem 'rspec-rails', group: [:development, :test]

## server
gem 'puma'
gem 'mina'
gem 'mina-puma', require: false, github: 'growthrepublic/mina-puma'

# Vagrant
file 'Vagrantfile', Net::HTTP.get(URI('https://bitbucket.org/growthrepublic/rails-template/raw/master/Vagrantfile'))

# Default config files
file 'config/database.yml', <<-CODE
default: &default
  adapter: postgresql
  encoding: unicode
  pool: 5

development:
  <<: *default
  database: #{@app_name}_development

test:
  <<: *default
  database: #{@app_name}_test

production:
  <<: *default
  database: #{@app_name}_production
  username: <%= ENV['DATABASE_USERNAME'] %>
  password: <%= ENV['DATABASE_PASSWORD'] %>
CODE

git :init
git add: '.'
git commit: %Q{ -m 'Initial commit'}

# Initialize Vagrant
run 'vagrant up' if yes?('Do you want to run Vagrant now?')
